# Assignment 7:  Mo' Beta

**Checkpoint: Wednesday, Dec 4th, 10AM**

**Due: Final Exam Week**

This is the final assignment for EE185. You will be building a beta version
of the shape, starting from the alpha you built in Assignment 6 and
applying what you've learned and consensus on how it should change.

As in Assignment 6, You'll work in subgroups on different aspects 
of the design and bring it all together as a group. We'll have a checkpoint
on the Wednesday after Thanksgiving (more details below) and you'll 
present the completed design during final exam week. We have two different
exam periods; we will spend one visiting the Box Shop and the second
on the design. 

## Shape and Design

We agreed to change the shape of the body, following the geometry in the
acrylic piece Charlie showed in class. This body is narrower and the wide
end has a wider angle, such that it extend past the wings less. 
At the weekly workshop, Pearl sketched a family of possible wing shapes.

## Subgroups and Assignments

We are breaking the team into five subgroups:

  1. Illumination/Wings (*Michal, Tim*, Claire, David): Choose three wing
     designs from [those Pearl sketched](wings.svg) ([PDF](wings.pdf)). Add 
     etching to wings, apply dichroic 
     film. Add brackets to all 3 wings so they can be interchangably attached. 
     Cleanly (not tape) attach LEDs to 1, 2, or 3 edges of the wings, exploring 
     how this affects their appearance. 
  2. Motion (*Will, Claire*, Lee): Design mechanism for wing motion. Each
     wing should be separately controllable. Holding a stationary position
     should not draw significant power. Working with body/installation 
     team, decide whether mechansism are internal or external.
  3. Computing (*Mihir, Sean*, Michal, Tim): Use a Stanford Maxwell board
     to program the motor controls and LEDs. Wings should move smoothly
     across their entire travel. Write at least 3 different LED patterns.
     Note that which wings you are using will likely be a parameter; define
     a common API or abstraction (e.g., a planar representation of color
     which is mapped to LEDs). Using a scope, check and verify PWM signals
     to wings. Correct non-uniform brightness from LEDs in software (e.g.,
     if ends  or tips are brighter, lower the brightness of those LEDs).
  4. Power/Interior/Illumination (*David, Kelly*, Sean): Determine how
     board and LEDs are mounted internally and wires route to wings.
     Decide on placement of internal LEDs to allow as close as possible to
     uniform brightness on body panels. Calculate power budget. Verify
     integrity of signals to LED strips and motor. Fix circuits to remove
     any noise observed.
  5. Body/Installation (*Andrea, Lee*, Will): Design and build body. Decide 
     material for top plate. Work with mechanism team on how components
     are secured and whether mechanisms are internal or external. 
     Remove all visible bolts. For the lower 4 faces, try two designs:
     one where plates are flush and one where there are large gaps.

If you need to purchase some small supplies (fasteners, etc.), please
keep all of your receipts. You have per-person $50 budget. 
If you need more expensive
materials, please email the staff list. 

**If you want materials that have some lead time, decide on this 
quickly so you'll have them.
This is especially true for motors, gears and belts.**

Finally, if you're blocking on something, please feel free to reach out to
the -staff list and we will help you however we can.

## Handing In

Come to class for the checkpoint on Wednesday, Dec 4th, and come to the
final exam period to discuss the final design.

 


