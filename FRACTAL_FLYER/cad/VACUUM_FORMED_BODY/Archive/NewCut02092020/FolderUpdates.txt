
Feb09 - 2020

- Updated the V6Mold
	Updated the draft angles to be 8 degrees for:
		1. straight draft
		2. babylip 
		3. bottom extrude 

	The babylip thickness went from:
		.125" -> .1" in thickness

- Updated the Assembly
	Changed the planes from which the operations start and END
		1. Operation 1 ends from .125" from the bottom Lip
		2. Operation 2 starts from .25" from bottom lip

		Issues: 1. When changing from Flat end mill -> Ball end mill, 
			there is a cusp on the straight drafted portion.  
				- Can try and use the flat end mill for the straight portion? 
			2. Also should play with stepover / stepdown to get cleaner cuts 
			
		

