EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 4 6
Title "Tail Board"
Date "2020-11-01"
Rev "2.1"
Comp "Stanford University"
Comment1 "Schematic for FLIGHT art installation tail board."
Comment2 "Tail board is on a Fractal Flyer, provides power and control."
Comment3 ""
Comment4 ""
$EndDescr
Wire Wire Line
	5185 2405 4400 2405
Wire Wire Line
	5180 2270 4400 2270
Wire Wire Line
	5185 2135 4400 2135
Wire Wire Line
	4365 4210 4365 4555
Wire Wire Line
	5145 4075 4365 4075
Wire Wire Line
	6020 4080 7805 4080
Wire Wire Line
	7805 4515 7805 4665
Wire Wire Line
	7855 4080 7805 4080
$Comp
L Device:C C42
U 1 1 617FC2E7
P 7805 4365
F 0 "C42" H 7920 4411 50  0000 L CNN
F 1 "0.1uF" H 7920 4320 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 7843 4215 50  0001 C CNN
F 3 "~" H 7805 4365 50  0001 C CNN
F 4 "GMC10X7R104M16NT" H 7805 4365 50  0001 C CNN "Manufacturer PN"
F 5 "CAL-CHIP ELECTRONICS, INC." H 7805 4365 50  0001 C CNN "Manufacturer"
F 6 "2571-GMC10X7R104M16NTTR-ND" H 7805 4365 50  0001 C CNN "Digikey PN"
F 7 "CAP CER 0.1UF 16V X7R 0603" H 7805 4365 50  0001 C CNN "Description"
	1    7805 4365
	1    0    0    -1  
$EndComp
$Comp
L Device:C C43
U 1 1 6178F198
P 7840 2560
F 0 "C43" H 7955 2606 50  0000 L CNN
F 1 "0.1uF" H 7955 2515 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 7878 2410 50  0001 C CNN
F 3 "~" H 7840 2560 50  0001 C CNN
F 4 "GMC10X7R104M16NT" H 7840 2560 50  0001 C CNN "Manufacturer PN"
F 5 "CAL-CHIP ELECTRONICS, INC." H 7840 2560 50  0001 C CNN "Manufacturer"
F 6 "2571-GMC10X7R104M16NTTR-ND" H 7840 2560 50  0001 C CNN "Digikey PN"
F 7 "CAP CER 0.1UF 16V X7R 0603" H 7840 2560 50  0001 C CNN "Description"
	1    7840 2560
	1    0    0    -1  
$EndComp
Wire Wire Line
	7840 2710 7840 2860
Text HLabel 4365 3940 0    50   Input ~ 0
PWM_M2_A
Text HLabel 4365 4075 0    50   Input ~ 0
PWM_M2_B
Text HLabel 7220 2410 2    50   Output ~ 0
M1B
Text HLabel 7090 3940 2    50   Output ~ 0
M2A
Text HLabel 7100 4215 2    50   Output ~ 0
M2B
Wire Wire Line
	7890 2275 7840 2275
Text HLabel 4400 2135 0    50   Input ~ 0
PWM_M1_A
Text HLabel 7890 2275 2    50   Input ~ 0
8V
Text HLabel 7855 4080 2    50   Input ~ 0
8V
Text HLabel 7805 4665 3    50   UnSpc ~ 0
GND
Text HLabel 4365 4555 3    50   UnSpc ~ 0
GND
Text HLabel 4400 2405 0    50   UnSpc ~ 0
GND
Text HLabel 7840 2860 3    50   UnSpc ~ 0
GND
Text HLabel 7230 2135 2    50   Output ~ 0
M1A
Wire Wire Line
	6015 4215 7100 4215
Wire Wire Line
	6015 3940 7090 3940
Wire Wire Line
	7805 4080 7805 4215
Connection ~ 7805 4080
Wire Wire Line
	5150 4210 4365 4210
$Comp
L tail-rescue:DRV8210_DRL U5
U 1 1 617FC2CF
P 5585 4335
F 0 "U5" H 5582 4955 50  0000 C CNN
F 1 "DRV8220_DRL" H 5582 4864 50  0000 C CNN
F 2 "motor12V:PDRV8220DRLR" H 5585 4335 50  0001 C CNN
F 3 "" H 5585 4335 50  0001 C CNN
F 4 "DRV8220DRLR" H 5585 4335 50  0001 C CNN "Manufacturer PN"
F 5 "Texas Instruments" H 5585 4335 50  0001 C CNN "Manufacturer"
F 6 "296-DRV8220DRLRTR-ND" H 5585 4335 50  0001 C CNN "Digikey PN"
F 7 "20-V, 1-A H-bridge motor driver" H 5585 4335 50  0001 C CNN "Description"
	1    5585 4335
	1    0    0    -1  
$EndComp
Wire Wire Line
	5150 3940 4365 3940
Text HLabel 4400 2270 0    50   Input ~ 0
PWM_M1_B
Wire Wire Line
	6055 2275 7840 2275
Wire Wire Line
	6050 2410 7220 2410
$Comp
L tail-rescue:DRV8210_DRL U6
U 1 1 617EE370
P 5620 2530
F 0 "U6" H 5617 3150 50  0000 C CNN
F 1 "DRV8220_DRL" H 5617 3059 50  0000 C CNN
F 2 "motor12V:PDRV8220DRLR" H 5620 2530 50  0001 C CNN
F 3 "" H 5620 2530 50  0001 C CNN
	1    5620 2530
	1    0    0    -1  
$EndComp
Wire Wire Line
	6050 2135 7230 2135
Wire Wire Line
	7840 2275 7840 2410
Connection ~ 7840 2275
$EndSCHEMATC
