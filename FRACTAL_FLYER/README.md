# Packard Interactive Light Sculpture Project

This directory stores all of the files, notes, and technical documents
for the Fractal Flyers (the physical art pieces) of the  Packard Interactive 
Light Sculpture Project. 

The directory structure:
  - cad: CAD drawings and designs for the piece,
  - datasheets: technical documentation of parts the piece uses,
  - design-docs: documentation of design decisions and technical components of the Fractal Flyer,
  - instructions: writeups of how to make different parts of the Fractal Flyer,

The Drawing Tree of the FRACTAL\_FLYER is in this google doc:
https://docs.google.com/document/d/1t9YB293jo6p6bc5lDQ5S1L3p2o_mtnMwk6ygtS_yizA/edit
The drawing tree helps us codify our shared vision of the final product.
It is a living document; please modify and elaborate as we solidify our designs.
