/* 
 * Defines the main pipeline for data transmission 
 * from the engine to the core simulator process. 
 * 
 * Creates named pipes into each fractal flyer's
 * filesystem, reads in python configuration data
 * spawns an interpreter and seperate process for 
 * each flyer, which write to a socket connected
 * to the core simulator process.
 *
 */ 

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/stat.h>
#include <vector>
#include <string>
#include <fcntl.h>
#include <Python.h>
#include <iostream> 
#include <errno.h> 
#include <sys/types.h>
#include <sys/wait.h>
#include <mutex>
#include <sys/prctl.h>
#include <thread>

#include "flight_server.h"

using namespace std; 

#define NUM_FF NUM_FLYERS //Change to 76. 
#define BUF_LENGTH 1000 

struct flyer {
    pid_t pid; //Pid associated with the flyer's process. 
};  

static const char* directory = "/tmp/fractal_flyers";
static const char *data_dir = "../../ui/data/outputs.json";

static int num_flyers;
static struct flyer* flyers;
static pid_t server;

/* Signal Handler run by server to shutdown any
 * open FIFOS if not shutdown by parent on death
 * of server 
 */
static void end_process(int signo) {
    for (int i =0 ; i < num_flyers; i++) { 
        char temparray[100];
        snprintf(temparray, 99, "%s/flyer%d",directory,i);
        remove(temparray);
    }
    remove(directory);
    exit(0);
}

/* Spawns each interpreter and runs code to 
 * define library functions in a local frame 
 */
void spawn_process (int flyer_num) {
    prctl(PR_SET_PDEATHSIG, SIGKILL); 
    char initcode[256];
    sprintf(initcode,  "import simLib\n"
                       "LEFT = 1\n"
                       "RIGHT = 0\n"
                       "simLib.init(%d);\n"
                       "FF = %d\n"
                       "def body_leds(a):\n"
                       "\tsimLib.body_leds(FF,a)\n"
                       "def wing_leds(a,b,c,d):\n"
                       "\tsimLib.wing_leds(FF,a,b,c,d)\n"
                       "def wing_angle(a,b):\n"
                       "\tsimLib.wing_angle(FF,a,b);\n"
                       ,flyer_num,flyer_num);
    Py_Initialize();
    PyRun_SimpleString(initcode);

    char temparray[100];
    sprintf(temparray,"%s/flyer%d",directory,flyer_num);
    Py_Initialize();
    while(true){
        char buf[BUF_LENGTH] = {'\0'}; 
        int np = open(temparray,O_RDONLY);
        read(np, buf, BUF_LENGTH);
        PyRun_SimpleString(buf);
        close(np);
    }
    exit(0);
} 

/* Sets up a specific flyer struct with its own named pipe. */ 
void create_fifo (int argc, char *argv[], int flyer_num, struct flyer &new_flyer)
{
    struct stat statbuf;
    if (stat(directory, &statbuf)) {
        int temp = mkdir(directory,0777);
    }
    char temparray[100];
    sprintf(temparray,"%s/flyer%d",directory,flyer_num);
    int fifo_ret = mkfifo(temparray, 0666); 
}

struct flyer spawn_new_flyer(int argc, char *argv[], int flyer_num)
{ 
    struct flyer new_flyer;
    create_fifo (argc, argv, flyer_num, new_flyer); 
    new_flyer.pid = fork(); 
    if(new_flyer.pid == 0)
        spawn_process (flyer_num); 
    return new_flyer; 
} 

/* Runs server to handle all the flyer requests
 */
pid_t spawn_server(){
    pid_t pid = fork();
    if(pid == 0){
        signal(SIGTERM, end_process);
        prctl(PR_SET_PDEATHSIG, SIGTERM); //runs signal on death of parent 
        std::thread server (flight_server_start);
        //supposed to call the simulator which has shared access to 
        //flyerStates and flyerLock
        //std::thread sim (simCode);
        //sim.join();
        server.join();
        exit(0);
    }
    return pid;
}
//Generates all of the processes
void create_data_pipeline(int argc, char *argv[])
{
  server = spawn_server();

  ofstream outputs(data_dir, ofstream::trunc);
  outputs << "[" << endl;
  for (int i = 0; i < num_flyers; i++)
  {
    flyers[i] = spawn_new_flyer(argc, argv, i);
    outputs << "\"/tmp/fractal_flyers/flyer" + std::to_string(i) + "\"," << endl;
  }
  outputs << "]" << endl;
  outputs.close();
}

/* Signal handler for main thread to kill all 
 * children and close the open FIFOs
 */
static void kill_all_processes(int signo){
    for (int i =0 ; i < num_flyers; i++) { 
        kill(flyers[i].pid, SIGKILL);
        waitpid(flyers[i].pid, NULL, 0);
        char temparray[100];
        sprintf(temparray,"%s/flyer%d", directory, i);
        remove(temparray);
    }
    kill(server, SIGKILL);
    waitpid(server, NULL, 0);
    remove(directory);
    exit(0);
}

/* 
 * Main takes in an initialization directory of 
 * the basic python framework from the caller. 
 */ 

//TODO(SD): Eventually define an init directory to pass into the process. 
int main(int argc, char *argv[]) 
{
    num_flyers = flight_server_num_flyers();
    flyers = (struct flyer*)malloc(sizeof(struct flyer) * num_flyers);
    
    create_data_pipeline(argc, argv);
    signal(SIGINT,kill_all_processes);
    signal(SIGTERM,kill_all_processes);
    signal(SIGQUIT,kill_all_processes);
    signal(SIGHUP,kill_all_processes);
    signal(SIGSEGV,kill_all_processes);
    
    char command;
    int flyer;
    //interactive console commands
    cout << "k \tkills all processes" << endl
         << "r id \trestart fractal flyer with id" << endl
         << "h \thelp" << endl;
    while (1) {
        scanf("%c",&command);
        if (command == 'k') {
            kill_all_processes(-1);
        } else if(command == 'r') {
            int count = scanf(" %d",&flyer);
            if (count != 1) {
  	        printf("usage: r ID\n");
		continue;
	    } else if (flyer >= num_flyers || flyer < 0) {
    	        printf("Invalid flyer id: %d\n", flyer);
                continue;
            } else {
	      printf("Restarting flyer %d\n", flyer);
	      kill(flyers[flyer].pid,SIGKILL);
	      flyers[flyer].pid = fork();
	      if (flyers[flyer].pid == 0) {
                spawn_process(flyer);
	      }
	    }
        } else if(command == 'h') {
            cout << "k \tkills all processes" << endl;
            cout << "r id \trestart fractal flyer with id" << endl;
            cout << "h \thelp" << endl;
        }
    }
    return 0;
}
