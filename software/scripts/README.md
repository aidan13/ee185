# FLIGHT: Packard Interactive Light Sculpture Project

This directory stores Python scripts and libraries for Fractal Flyers.
It has the following example scripts. To install a script, copy it
to CIRCUITPY and name it `code.py`.
  - `flapping-i2c.py`: A script that flaps the left and right wings 
  together, from -45 to 45 degrees. It assumes the left and right
  wing sensors are I2C LIS3DH and the body sensor is an LISD3DH over SPI. 
  The left sensor has I2C address 0x19 and the right sensor has 0x17.
  The center sensor has a chip select line of D6. The left motor is on
  pins A1/A3 and the right motor is on pins RX/TX.

It also has these library modules:
  - `adafruit_lis3dh.py` is the Adafruit library for the LIS3DH sensor.
  Put it in the `lib` directory of CIRCUITPY.
  - `neopixel.mpy` is the Adafruit library for controlling neopixel
  LEDs. Put it in the `lib` directory of CIRCUITPY.
  - `adafruit_bus_device` is the Adafruit library for using the SPI
  and I2C buses. Put this direcotry in the `lib` directory of
  CIRCUITPY.
